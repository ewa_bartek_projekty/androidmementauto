package com.front.androidmementauto.webservice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.front.androidmementauto.entities.AddWorkDto;
import com.front.androidmementauto.entities.UserDto;
import com.front.androidmementauto.entities.VehicleDto;
import com.front.androidmementauto.entities.VehicleUserDto;
import com.front.androidmementauto.entities.WorkDto;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ConnectionHelper {
    private final ObjectMapper objectMapper;
    private final ConnectionBean connectionBean;
    private final String TAG = "ConnectionHelper";
    public ConnectionHelper(){
        objectMapper = new ObjectMapper();
        connectionBean = new ConnectionBean("http://192.168.43.77:8080/api");
    }


    public UserDto registerUser(UserDto userDto) throws IOException {
        String registerData = connectionBean.postObject(userDto, "/user/register");
        if (registerData != null && !registerData.isEmpty()){
            UserDto user = objectMapper.readValue(registerData, UserDto.class);
            return user;
        }
        return null;
    }

    public List<VehicleDto> showUserVehicles(String authToken) throws IOException {
        connectionBean.setAuthHeader(authToken);
        String userVehicles = connectionBean.postGet("/vehicles");
        if (userVehicles != null && !userVehicles.isEmpty()){
            List<VehicleDto> vehicles = objectMapper.readValue(userVehicles, new TypeReference<List<VehicleDto>>(){});
            return vehicles;
        }
        return new ArrayList<>();
    }

    public String loginUser(String username, String password) throws IOException {
        HashMap<String, Object> hashMapParameters = new HashMap<>();
        hashMapParameters.put("username", username);
        hashMapParameters.put("password", password);
        ConnectionBean differentConnectionBean = new ConnectionBean("http://192.168.43.77:8080");
        String loginProcess = differentConnectionBean.postXWwwFormUrlEncoded("/login", hashMapParameters);
        return loginProcess;
    }

    public VehicleDto addUserVehicle(String authToken, VehicleDto vehicleDto) throws IOException {
        connectionBean.setAuthHeader(authToken);
        String json = connectionBean.postObject(vehicleDto, "/saveVehicle");
        if(json!=null && !json.isEmpty()){
            VehicleDto vehicleDto1 = objectMapper.readValue(json, VehicleDto.class);
            return  vehicleDto1;
        }
        return null;
    }

    public VehicleDto changeOwner(String authToken, VehicleUserDto vehicleDto) throws IOException {
        connectionBean.setAuthHeader(authToken);
        String json = connectionBean.postObject(vehicleDto, "/changeOwner");
        if(json!=null && !json.isEmpty()){
            VehicleDto vehicleDto1 = objectMapper.readValue(json, VehicleDto.class);
            return  vehicleDto1;
        }
        return null;
    }



    public VehicleDto showUserVehicle(String authToken, Long id) throws IOException{
        connectionBean.setAuthHeader(authToken);
        String json = connectionBean.postGet("/showVehicle/"+id);
        if(json!=null && !json.isEmpty()){
            VehicleDto vehicleDto = objectMapper.readValue(json, VehicleDto.class);
            return vehicleDto;
        }
        return null;
    }

    public UserDto findUserByLogin(String autToken, String login) throws IOException{
        connectionBean.setAuthHeader(autToken);
        String json = connectionBean.postGet("/user/findUserEntityByLogin/"+ login);
        if(json!=null && !json.isEmpty()){
            UserDto userDto = objectMapper.readValue(json, UserDto.class);
            return userDto;
        }
        return null;
    }

    public VehicleDto editVehicle(String authToken, VehicleUserDto vehicleDto) throws IOException{
        connectionBean.setAuthHeader(authToken);
        String json = connectionBean.postObject(vehicleDto, "/editVehicle");
        if(json!=null && !json.isEmpty()){
            VehicleDto vehicleDto1 = objectMapper.readValue(json, VehicleDto.class);
            return  vehicleDto1;
        }
        return null;
    }

    public List<WorkDto> findWorksById(String authToken, Long id) throws IOException {
        connectionBean.setAuthHeader(authToken);
        String userVehicles = connectionBean.postGet("/findWorksById/"+id);
        if (userVehicles != null && !userVehicles.isEmpty()){
            List<WorkDto> workDtoList = objectMapper.readValue(userVehicles, new TypeReference<List<WorkDto>>(){});
            return workDtoList;
        }
        return new ArrayList<>();
    }

    public List<WorkDto> findInsurancesById(String authToken, Long id) throws IOException {
        connectionBean.setAuthHeader(authToken);
        String userVehicles = connectionBean.postGet("/findInsurancesById/"+ id);
        if (userVehicles != null && !userVehicles.isEmpty()){
            List<WorkDto> workDtoList = objectMapper.readValue(userVehicles, new TypeReference<List<WorkDto>>(){});
            return workDtoList;
        }
        return new ArrayList<>();
    }

    public WorkDto saveWork(String authToken, AddWorkDto workDto) throws IOException {
        connectionBean.setAuthHeader(authToken);
        String json = connectionBean.postObject(workDto, "/saveWork");
        if(json!=null && !json.isEmpty()){
            WorkDto workDto1 = objectMapper.readValue(json, WorkDto.class);
            return  workDto1;
        }
        return null;
    }

    public WorkDto saveInsurance(String authToken, AddWorkDto workDto) throws IOException {
        connectionBean.setAuthHeader(authToken);
        String json = connectionBean.postObject(workDto, "/saveInsurance");
        if(json!=null && !json.isEmpty()){
            WorkDto workDto1 = objectMapper.readValue(json, WorkDto.class);
            return  workDto1;
        }
        return null;
    }


}
