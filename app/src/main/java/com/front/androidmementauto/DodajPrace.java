package com.front.androidmementauto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.front.androidmementauto.data.Singleton;
import com.front.androidmementauto.entities.AddWorkDto;
import com.front.androidmementauto.entities.TokenDto;
import com.front.androidmementauto.webservice.ConnectionHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DodajPrace extends AppCompatActivity {

    private static final String TAG = "DodajPrace";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_prace);

        Singleton singleton = Singleton.getInstance();
        Long id = singleton.getVehicleDto().getId();//id samochodu

        EditText name = findViewById(R.id.name);
        EditText category = findViewById(R.id.category);
        EditText workDate = findViewById(R.id.workDate);
        EditText hour = findViewById(R.id.hour);
        Button zapisz = findViewById(R.id.saveWork);

        zapisz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable date1 = workDate.getText();
                Date date= null;
                AddWorkDto addWorkDto = new AddWorkDto();
                addWorkDto.setVehicle(id);
                try {
                    date = new SimpleDateFormat("yyyy-MM-dd").parse(date1.toString());
                    addWorkDto.setDate(date);
                    addWorkDto.setHour(String.valueOf(hour.getText()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                addWorkDto.setCategory(String.valueOf(category.getText()));
                addWorkDto.setWorkName(String.valueOf(name.getText()));
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        ConnectionHelper connectionHelper = new ConnectionHelper();
                        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                        String output = sharedPreferences.getString("token", "defaultValue");
                        try {
                            TokenDto tokenDto = new ObjectMapper().readValue(output, TokenDto.class);
                            connectionHelper.saveWork(tokenDto.getAccessToken(), addWorkDto);
                        }
                        catch (Exception e){
                            Log.e(TAG, "run: ", e);
                        }
                    }
                });
                try {
                    Thread.sleep(1000);
                    //finish();
                    Intent intent = new Intent(getApplicationContext(), ActivityUserVehicles.class);
                    startActivity(intent);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }

            }
        });


    }
}