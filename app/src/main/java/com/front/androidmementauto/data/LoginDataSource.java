package com.front.androidmementauto.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.se.omapi.Session;

import com.front.androidmementauto.data.model.LoggedInUser;
import com.front.androidmementauto.webservice.ConnectionBean;
import com.front.androidmementauto.webservice.ConnectionHelper;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {
    ConnectionHelper connectionHelper;
    public Result<LoggedInUser> login(String username, String password) {

        try {

            final String token = null;

            Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try  {
                        connectionHelper = new ConnectionHelper();
                        String token = connectionHelper.loginUser(username, password);
                        Singleton singleton = Singleton.getInstance();
                        singleton.setToken(token);
                        // TODO: handle loggedInUser authentication

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
            thread.join();
                Singleton singleton = Singleton.getInstance();
                LoggedInUser fakeUser =
                        new LoggedInUser(
                                java.util.UUID.randomUUID().toString(),
                                username, singleton.getToken());


                return new Result.Success<>(fakeUser);



        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}