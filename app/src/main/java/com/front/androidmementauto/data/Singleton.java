package com.front.androidmementauto.data;

import com.front.androidmementauto.entities.VehicleDto;
import com.front.androidmementauto.entities.WorkDto;

import java.util.List;

public class Singleton {
    private static Singleton singleton;
    private String token;
    private VehicleDto vehicleDto;
    private Boolean uzupelniony;
    private List<WorkDto> workDtoList;
    private List<WorkDto> insuranceDtoList;
    public static Singleton getInstance(){
        if(singleton==null){
            singleton = new Singleton();
        }
        return singleton;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public VehicleDto getVehicleDto() {
        return vehicleDto;
    }

    public void setVehicleDto(VehicleDto vehicleDto) {
        this.vehicleDto = vehicleDto;
    }

    public Boolean getUzupelniony() {
        return uzupelniony;
    }

    public void setUzupelniony(Boolean uzupelniony) {
        this.uzupelniony = uzupelniony;
    }

    public List<WorkDto> getWorkDtoList() {
        return workDtoList;
    }

    public void setWorkDtoList(List<WorkDto> workDtoList) {
        this.workDtoList = workDtoList;
    }

    public List<WorkDto> getInsuranceDtoList() {
        return insuranceDtoList;
    }

    public void setInsuranceDtoList(List<WorkDto> insuranceDtoList) {
        this.insuranceDtoList = insuranceDtoList;
    }
}
