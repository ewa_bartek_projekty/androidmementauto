package com.front.androidmementauto.data.car.cards;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.front.androidmementauto.ActivityUserCarDetails;
import com.front.androidmementauto.R;
import com.front.androidmementauto.entities.VehicleDto;

import java.io.ByteArrayInputStream;
import java.util.Base64;
import java.util.List;
import java.util.Random;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder>{

    private final List<VehicleDto> carDataList;
    private Context context;

    public CarAdapter(List<VehicleDto> carDataList, Activity activity){
        context = activity;
        this.carDataList = carDataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.car_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final VehicleDto carData = carDataList.get(position);
        holder.carBrand.setText(carData.getBrand());
        holder.carModel.setText(carData.getModel());
        holder.cardView.setCardBackgroundColor(randomColor());
        if (carData.getCarPicture() != null){
            holder.carImage.setImageBitmap(byteToBitmap(carData.getCarPicture()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "ID: " + carData.getId(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ActivityUserCarDetails.class);
                intent.putExtra("param",String.valueOf(carData.getId()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return carDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView carImage;
        TextView carBrand;
        TextView carModel;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.carCardView);
            carImage = itemView.findViewById(R.id.carImage);
            carBrand = itemView.findViewById(R.id.carBrand);
            carModel = itemView.findViewById(R.id.carModel);
        }
    }

    private int randomNum(){
        final int min = 1;
        final int max = 255;
        return new Random().nextInt((max - min) + 1) + min;
    }
    private int randomColor(){
        return Color.argb(100, randomNum(), randomNum(), randomNum());
    }

    private Bitmap byteToBitmap(String bytes64) {
        byte [] bytes = Base64.getDecoder().decode(bytes64);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

}
