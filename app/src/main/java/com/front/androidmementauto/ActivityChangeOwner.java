package com.front.androidmementauto;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.front.androidmementauto.data.Singleton;
import com.front.androidmementauto.entities.TokenDto;
import com.front.androidmementauto.entities.UserDto;
import com.front.androidmementauto.entities.VehicleDto;
import com.front.androidmementauto.entities.VehicleUserDto;
import com.front.androidmementauto.webservice.ConnectionHelper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ActivityChangeOwner extends AppCompatActivity {

    private final static String TAG = "ActivityChangeOwner";
    private TextView currentOwner;
    private TextView newOwner;
    private Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_owner);
        currentOwner = findViewById(R.id.current_owner);
        newOwner = findViewById(R.id.new_owner);
        save = findViewById(R.id.save);

        VehicleDto vehicleDto = Singleton.getInstance().getVehicleDto();
        currentOwner.setText(vehicleDto.getOwner().getLogin());


        VehicleUserDto finalVehicleDto = new VehicleUserDto();
        finalVehicleDto.setBrand(vehicleDto.getBrand());
        finalVehicleDto.setId(vehicleDto.getId());
        finalVehicleDto.setMileage(vehicleDto.getMileage());
        finalVehicleDto.setModel(vehicleDto.getModel());
        finalVehicleDto.setCarPicture(vehicleDto.getCarPicture());
        finalVehicleDto.setProductionYear(vehicleDto.getProductionYear());
        finalVehicleDto.setOwner(vehicleDto.getOwner().getId());
        finalVehicleDto.setWorks(vehicleDto.getWorks());


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        ConnectionHelper connectionHelper = new ConnectionHelper();
                        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                        String output = sharedPreferences.getString("token", "defaultValue");
                        try {
                            TokenDto tokenDto = new ObjectMapper().readValue(output, TokenDto.class);
                            UserDto userDto = connectionHelper.findUserByLogin(tokenDto.getAccessToken(), newOwner.getText().toString());

                            if (userDto != null) {
                                Long newUserId = userDto.getId();
                                finalVehicleDto.setOwner(newUserId);
                                VehicleDto vehicleDtoResult = connectionHelper.changeOwner(tokenDto.getAccessToken(), finalVehicleDto);
                                Intent intent = new Intent(getApplicationContext(), ActivityUserVehicles.class);
                                startActivity(intent);
                                Log.i(TAG, "Dodano auto: " + vehicleDtoResult.toString());
                            } else {
                                System.out.println("nie ma takiego usera");
                            }
                        } catch (JsonProcessingException e) {
                            Log.e(TAG, "error: ", e);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}