package com.front.androidmementauto;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.front.androidmementauto.data.Singleton;
import com.front.androidmementauto.entities.TokenDto;
import com.front.androidmementauto.entities.VehicleDto;
import com.front.androidmementauto.entities.WorkDto;
import com.front.androidmementauto.webservice.ConnectionHelper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.List;

public class ActivityUserCarDetails extends AppCompatActivity {

    private static final String TAG = "ActivityUserCarDetails";
    private TextView model;
    private TextView brand;
    private TextView mileage;
    private TextView productionYear;
    private TextView login;
    private Button edit;
    private Button change_owner;
    private ImageView carImage;
    private ScrollView scrollView2;
    private ScrollView scrollView3;
    private Button dodajUbezpieczenie;
    private Button dodajPrace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_car_details);

        model = findViewById(R.id.model);
        brand = findViewById(R.id.marka);
        mileage = findViewById(R.id.przebieg);
        productionYear= findViewById(R.id.rok_produkcji);
        login = findViewById(R.id.login2);
        edit = findViewById(R.id.edit);
        change_owner = findViewById(R.id.change_owner);
        carImage = findViewById(R.id.car_details_image);
        scrollView2 = findViewById(R.id.scrollView2);
        scrollView3 = findViewById(R.id.scrollView3);
        dodajUbezpieczenie = findViewById(R.id.dodaj_ubezpieczenie);
        dodajPrace = findViewById(R.id.dodaj_prace);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Intent myIntent = getIntent(); // gets the previously created intent
                String id = myIntent.getStringExtra("param");
                ConnectionHelper connectionHelper = new ConnectionHelper();
                SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                String output = sharedPreferences.getString("token", "defaultValue");
                try {
                    TokenDto tokenDto = new ObjectMapper().readValue(output, TokenDto.class);
                    VehicleDto vehicleDto = connectionHelper.showUserVehicle(tokenDto.getAccessToken(), Long.parseLong(id));
                    if(vehicleDto!=null){
                        Singleton singleton = Singleton.getInstance();
                        singleton.setVehicleDto(vehicleDto);
                        singleton.setUzupelniony(true);
                    }
                    List<WorkDto> workDtoList = connectionHelper.findInsurancesById(tokenDto.getAccessToken(), Long.parseLong(id));
                    if(workDtoList!=null){
                        Singleton singleton = Singleton.getInstance();
                        singleton.setWorkDtoList(workDtoList);
                    }

                    List<WorkDto> insuranceDtoList = connectionHelper.findWorksById(tokenDto.getAccessToken(), Long.parseLong(id));
                    if(insuranceDtoList!=null){
                        Singleton singleton = Singleton.getInstance();
                        singleton.setInsuranceDtoList(insuranceDtoList);
                    }
                } catch (JsonProcessingException e) {
                    Log.e(TAG, "error: ", e);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
        Singleton singleton = Singleton.getInstance();
        model.setText(singleton.getVehicleDto().getModel());
        brand.setText(singleton.getVehicleDto().getBrand());
        mileage.setText(String.valueOf(singleton.getVehicleDto().getMileage()));
        productionYear.setText(singleton.getVehicleDto().getProductionYear());
        login.setText(singleton.getVehicleDto().getOwner().getLogin());
        if(singleton.getVehicleDto().getCarPicture() != null){
            carImage.setImageBitmap(byteToBitmap(singleton.getVehicleDto().getCarPicture()));
        }
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setVerticalScrollBarEnabled(true);
        if(singleton.getWorkDtoList()!=null && !singleton.getWorkDtoList().isEmpty()) {
            for (WorkDto workDto : singleton.getWorkDtoList()) {
                TextView textView = new TextView(this);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String dateString = format.format(workDto.getDate());
                textView.setText(workDto.getWorkName() + " " + dateString + " " + workDto.getHour());
                linearLayout.addView(textView);
                View view = new View(this);
                view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 50));
                linearLayout.addView(view);
            }
            scrollView2.addView(linearLayout);
        }

        if(singleton.getInsuranceDtoList()!=null && !singleton.getInsuranceDtoList().isEmpty()) {
            LinearLayout linearLayout3 = new LinearLayout(this);
            linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            linearLayout3.setOrientation(LinearLayout.VERTICAL);
            for (WorkDto workDto : singleton.getInsuranceDtoList()) {
                TextView textView = new TextView(this);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String dateString = format.format(workDto.getDate());
                textView.setText(workDto.getWorkName() + " " + dateString + " " + workDto.getHour());
                linearLayout3.addView(textView);

            }
            scrollView3.addView(linearLayout3);
        }

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getApplicationContext(), ActivityEditUserCar1.class);
                        startActivity(intent);
                    }
                });
            }
        });

        dodajUbezpieczenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DodajUbezpieczenie.class);
                startActivity(intent);
            }
        });

        dodajPrace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DodajPrace.class);
                startActivity(intent);
            }
        });

        change_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityChangeOwner.class);
                startActivity(intent);
            }
        });

    }
    private ActivityResultLauncher<String[]> galleryActivityLauncher = registerForActivityResult(new ActivityResultContracts.OpenDocument(), new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri result) {
            if (result != null) {
                carImage.setImageURI(result);
            } else {
                Log.d(TAG, "onActivityResult: the result is null for some reason");
            }
        }
    });

    private String imageToBytes(){
        carImage.invalidate();
        BitmapDrawable bitmapDrawable = (BitmapDrawable) carImage.getDrawable();
        Bitmap bitmap = bitmapDrawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return Base64.getEncoder().encodeToString(byteArray);
    }

    private Bitmap byteToBitmap(String bytes64) {
        byte [] bytes = Base64.getDecoder().decode(bytes64);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}