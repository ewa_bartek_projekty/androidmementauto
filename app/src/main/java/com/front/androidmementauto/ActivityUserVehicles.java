package com.front.androidmementauto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.front.androidmementauto.data.car.cards.CarAdapter;
import com.front.androidmementauto.entities.TokenDto;
import com.front.androidmementauto.entities.VehicleDto;
import com.front.androidmementauto.webservice.ConnectionHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ActivityUserVehicles extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_vehicles);


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                RecyclerView carRecycler = (RecyclerView) findViewById(R.id.carRecycler);
                carRecycler.setHasFixedSize(true);
                carRecycler.setLayoutManager(new LinearLayoutManager(getParent()));
                ConnectionHelper connectionHelper = new ConnectionHelper();

                SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                String output = sharedPreferences.getString("token", "defaultValue");
                List<VehicleDto> vehicleDtoList = new ArrayList<>();
                try {
                    TokenDto tokenDto = new ObjectMapper().readValue(output, TokenDto.class);
                    vehicleDtoList = connectionHelper.showUserVehicles(tokenDto.getAccessToken());
                } catch (RuntimeException e) {
                    if (e.toString().contains("403")) {
                        System.out.println("Wyloguj");
                        sharedPreferences.edit().remove("token").commit();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        return;
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }

                if (vehicleDtoList != null) {
                    List<VehicleDto> finalVehicleDtoList = vehicleDtoList;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            CarAdapter carAdapter = new CarAdapter(finalVehicleDtoList, ActivityUserVehicles.this);
                            carRecycler.setAdapter(carAdapter);
                        }
                    });
                }

            }
        });

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Intent intent = new Intent(getApplicationContext(), ActivityAddUserCar.class);
                            startActivity(intent);
                        } catch (Exception e) {

                        }
                    }
                });
            }
        });

    }
}