package com.front.androidmementauto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.front.androidmementauto.data.LoginRepository;
import com.front.androidmementauto.entities.SingletonLogin;
import com.front.androidmementauto.ui.login.LoginActivity;

public class MainActivity extends AppCompatActivity {

    private LoginRepository loginRepository;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton accountButton = findViewById(R.id.accountButton);
        TextView login1 = findViewById(R.id.login1);
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        boolean isSession=sharedPreferences.getString("token", "defaultValue").contains("accessToken");
        if(isSession){
            SingletonLogin singletonLogin = SingletonLogin.getInstance();
            login1.setText(singletonLogin.getUsername());
        }
        else {
            login1.setText("Zaloguj się!");
        }

        accountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                boolean isSession=sharedPreferences.getString("token", "defaultValue").contains("accessToken");
                if(!isSession)
                {
                    menuUserNotLogged(v);
                }
                else{
                    SingletonLogin singletonLogin = SingletonLogin.getInstance();
                    login1.setText(singletonLogin.getUsername());

                    PopupMenu popup = new PopupMenu(getApplicationContext(), v);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.popup1, popup.getMenu());

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getTitle().toString()){
                                case "Moje samochody":{
                                    Intent intent = new Intent(getApplicationContext(), ActivityUserVehicles.class);
                                    startActivity(intent);
                                    return true;
                                }
                                case "Wyloguj":{
                                    login1.setText("Zaloguj się!");
                                    SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                                    sharedPreferences=null;
                                    menuUserNotLogged(v);
                                    return true;
                                }
                                default:{
                                    return false;
                                }
                            }
                        }
                    });

                    popup.show();
                }
            }
        });
    }

    private void menuUserNotLogged(View v) {
        PopupMenu popup = new PopupMenu(getApplicationContext(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getTitle().toString()){
                    case "Rejestracja":{
                        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                        startActivity(intent);
                        return true;
                    }
                    case "Logowanie":{
                        Intent myIntet = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(myIntet);
                    }
                    default:{
                        return false;
                    }
                }
            }
        });

        popup.show();
    }
}