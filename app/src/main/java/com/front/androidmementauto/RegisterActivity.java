package com.front.androidmementauto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.front.androidmementauto.entities.UserDto;
import com.front.androidmementauto.ui.login.LoginActivity;
import com.front.androidmementauto.webservice.ConnectionHelper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

public class RegisterActivity extends AppCompatActivity {

    ConnectionHelper connectionHelper;
    private final String TAG = "RegisterActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        connectionHelper = new ConnectionHelper();

        Button registerButton = findViewById(R.id.register_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText login = ((EditText) findViewById(R.id.register_login));
                EditText password = ((EditText) findViewById(R.id.register_password));
                EditText name = ((EditText) findViewById(R.id.register_name));
                EditText surname = ((EditText) findViewById(R.id.register_surname));
                EditText email =  ((EditText) findViewById(R.id.register_email));
                boolean valid=true;
                if(login.getText().toString().length()==0){
                    login.setError("Podaj login!");
                    valid=false;
                }

                if(password.getText().toString().length()==0){
                    password.setError("Podaj hasło!");
                    valid=false;
                }

                if(name.getText().toString().length()==0){
                    name.setError("Podaj imie!");
                    valid=false;
                }

                if(surname.getText().toString().length()==0){
                    surname.setError("Podaj nazwisko!");
                    valid=false;
                }

                if(email.getText().toString().length()==0){
                    email.setError("Podaj email!");
                    valid=false;
                }
                if(!email.getText().toString().contains("@")){
                    email.setError("Podaj poprawny email!");
                    valid=false;
                }
                if(!valid){
                    return;
                }
                UserDto userDto = new UserDto();
                userDto.setLogin(login.getText().toString());
                userDto.setPassword(password.getText().toString());
                userDto.setName(name.getText().toString());
                userDto.setSurname(surname.getText().toString());
                userDto.setNotificationOn(((Switch) findViewById(R.id.isNotificationOn)).isChecked());
                userDto.setEmail(((EditText) findViewById(R.id.register_email)).getText().toString());

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            connectionHelper.registerUser(userDto);
                        } catch (IOException e) {
                            Log.e(TAG, "run: ", e);
                        }
                        Intent myIntet = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(myIntet);
//                        String status = connectionHelper.registerUser(userDto);
//                        if(status!=null && status.equals("ok")){
//                            Intent myIntet = new Intent(RegisterActivity.this, LoginActivity.class);
//                            startActivity(myIntet);
//                        }
//                        else {
//
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run(){
//                                    login.setError("Taki użytkownik już istnieje!");
//                                }
//                            });
//                        }
                    }
                });
            }
        });
    }
}