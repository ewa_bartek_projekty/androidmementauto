package com.front.androidmementauto;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.front.androidmementauto.data.Singleton;
import com.front.androidmementauto.entities.TokenDto;
import com.front.androidmementauto.entities.VehicleDto;
import com.front.androidmementauto.entities.VehicleUserDto;
import com.front.androidmementauto.webservice.ConnectionHelper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public class ActivityEditUserCar1 extends AppCompatActivity {

    private final String TAG = "ActivityEditUserCar";
    private TextView model;
    private TextView brand;
    private TextView productionYear;
    private TextView mileage;
    private ImageView carImage;
    private Button wyslij;
    private Long id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_car1);
        model = findViewById(R.id.model);
        brand = findViewById(R.id.brand);
        productionYear = findViewById(R.id.productionYear);
        mileage = findViewById(R.id.mileage);
        carImage = findViewById(R.id.car_edit_action_view);
        wyslij = (Button) findViewById(R.id.button);

        carImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryActivityLauncher.launch(new String[]{"image/*"});
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Singleton singleton = Singleton.getInstance();
                VehicleDto vehicleDto = singleton.getVehicleDto();
                id = vehicleDto.getId();
                brand.setText(vehicleDto.getBrand());
                model.setText(vehicleDto.getModel());
                productionYear.setText(vehicleDto.getProductionYear());
                mileage.setText(String.valueOf(vehicleDto.getMileage()));
                if(vehicleDto.getCarPicture() != null){
                    carImage.setImageBitmap(byteToBitmap(vehicleDto.getCarPicture()));
                }
            }
        });

        wyslij.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        VehicleUserDto vehicleDto = createVehicleDtoObject();
                        ConnectionHelper connectionHelper = new ConnectionHelper();
                        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                        String output = sharedPreferences.getString("token", "defaultValue");
                        try {
                            TokenDto tokenDto = new ObjectMapper().readValue(output, TokenDto.class);
                            VehicleDto vehicleDto1 = connectionHelper.editVehicle(tokenDto.getAccessToken(), vehicleDto);
                            if(vehicleDto1!=null){
                                Intent intent = new Intent(getApplicationContext(), ActivityUserVehicles.class);
                                startActivity(intent);
                                Log.i(TAG, "Dodano auto: " + vehicleDto1.toString());
                            }
                        } catch (JsonProcessingException e) {
                            Log.e(TAG, "error: ", e);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });
    }

    private VehicleUserDto createVehicleDtoObject() {
        VehicleUserDto vehicleDto = new VehicleUserDto();
        vehicleDto.setBrand(brand.getText().toString());
        vehicleDto.setModel(model.getText().toString());
        vehicleDto.setProductionYear( productionYear.getText().toString());
        vehicleDto.setMileage(Double.parseDouble(mileage.getText().toString()));
        vehicleDto.setId(id);
        vehicleDto.setCarPicture(imageToBytes());
        return vehicleDto;
    }

    private ActivityResultLauncher<String[]> galleryActivityLauncher = registerForActivityResult(new ActivityResultContracts.OpenDocument(), new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri result) {
            if (result != null) {
                carImage.setImageURI(result);
            } else {
                Log.d(TAG, "onActivityResult: the result is null for some reason");
            }
        }
    });

    private String imageToBytes(){
        carImage.invalidate();
        BitmapDrawable bitmapDrawable = (BitmapDrawable) carImage.getDrawable();
        Bitmap bitmap = bitmapDrawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return Base64.getEncoder().encodeToString(byteArray);
    }

    private Bitmap byteToBitmap(String bytes64) {
        byte[] bytes = Base64.getDecoder().decode(bytes64);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}