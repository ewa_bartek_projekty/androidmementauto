package com.front.androidmementauto.entities;

import java.util.Set;

import lombok.Data;

@Data
public class UserDto{
    private Long id;
    private String login;
    private String password;
    private String name;
    private String surname;
    private String profilePicture;
    private String email;
    private Boolean notificationOn;
    private Boolean enabled;
    private Set<GrantedAuthority> authorities;
    private String username;
    private Boolean accountNonLocked;
    private Boolean accountNonExpired;
    private Boolean credentialsNonExpired;

}
