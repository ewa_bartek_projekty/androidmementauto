package com.front.androidmementauto.entities;

import lombok.Data;

@Data
public class VehicleDto {
    private Long id;
    private String brand;
    private String model;
    private String productionYear;
    private Double mileage;
    private String carPicture;
    private Long works;
    private UserDto owner;
}
