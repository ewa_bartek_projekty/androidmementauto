package com.front.androidmementauto.entities;

public class SingletonLogin {
    public static SingletonLogin instance;
    private String username;

    public static SingletonLogin getInstance() {
        if (instance == null) {
            instance = new SingletonLogin();
        }
        return instance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
