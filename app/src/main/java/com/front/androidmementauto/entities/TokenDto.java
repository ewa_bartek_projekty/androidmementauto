package com.front.androidmementauto.entities;

import lombok.Data;

@Data
public class TokenDto {
    String accessToken;
    String refreshToken;
}
