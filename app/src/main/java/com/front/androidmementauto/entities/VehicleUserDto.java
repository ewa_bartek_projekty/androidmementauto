package com.front.androidmementauto.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleUserDto {
    private Long id;
    private String brand;
    private String model;
    private String productionYear;
    private Double mileage;
    private String carPicture;
    private Long works;
    private Long owner;
}
