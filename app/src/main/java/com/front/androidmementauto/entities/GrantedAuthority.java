package com.front.androidmementauto.entities;

import lombok.Data;

@Data
public class GrantedAuthority {
    private String authority;

}
