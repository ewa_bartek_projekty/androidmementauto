package com.front.androidmementauto.entities;

import java.util.Date;

import lombok.Data;

@Data
public class AddWorkDto {
    private Long id;
    private String workName;
    private String category;
    private Date date;
    private String hour;
    private Long vehicle;
}
